#include "main-sae-hmi-app.h"
#include <QApplication>
#include <QFile>
#include<QString>
#include <QDebug>
//--------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainSAEHMIApp app;

    qDebug() << "Loading application stylesheet";
    QFile file(":/styles/styles/style.qss");
    file.open(QFile::ReadOnly);
    QString stylesheet{QLatin1String(file.readAll())};
    app.setStyleSheet(stylesheet);

    app.show();
    qDebug() << "Application initialized";
    return a.exec();
}
//--------------------------------------------------------------------------------------------------

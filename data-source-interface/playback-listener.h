#ifndef PLAYBACK_LISTENER_H
#define PLAYBACK_LISTENER_H

#include "message-listener.h"
#include <QFile>
#include <QTimer>

class PlaybackListener: public IMessageListener
{
    public:
        static PlaybackListener* instance();

    private:
        explicit PlaybackListener(QObject* parent = nullptr);
        ~PlaybackListener() override;

        ListenerType type{PLAYBACK};

        QFile file;
        QTimer timer;
        QByteArray nextMessage;

    protected:
        void receiveData() override;

    public slots:
        void configure() override;
};

#endif // PLAYBACK_LISTENER_H

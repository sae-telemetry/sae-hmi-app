#ifndef CAN_LISTENER_H
#define CAN_LISTENER_H

#include "message-listener.h"
#include <QSerialPort>

class CANListener: public IMessageListener
{
    Q_OBJECT
    public:
        static CANListener* instance();

    private:
        explicit CANListener(QWidget* parent = nullptr);
        ~CANListener() override;

        void closePort();
        void sendXOFF();
        void sendXON();

        const ListenerType type{RADIO};
        QSerialPort serialPort;

    protected:
        void receiveData() override;

    signals:
        void publishError(QString error);

    public slots:
        void configure() override;

    private slots:
        void handleError(QSerialPort::SerialPortError serialPortError);
};

#endif // CAN_LISTENER_H

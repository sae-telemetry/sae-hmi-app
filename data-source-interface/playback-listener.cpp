#include "playback-listener.h"
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static PlaybackListener* inst{nullptr};
//--------------------------------------------------------------------------------------------------
PlaybackListener* PlaybackListener::instance()
{
    if(nullptr == inst)
    {
        qDebug() << "PlaybackListener - instantiating";
        inst = new PlaybackListener(nullptr);
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
PlaybackListener::PlaybackListener(QObject* parent) : IMessageListener(parent)
{
        qDebug() << "PlaybackListener - Creating instance";
    configure();
}
//--------------------------------------------------------------------------------------------------
PlaybackListener::~PlaybackListener()
{
    if (file.isOpen())
    {
        file.close();
    }
    qDebug() << "PlaybackListener - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------
void PlaybackListener::receiveData()
{
    if (nextMessage.size() >= static_cast<qint32>(messageSize))
    {
        qDebug() << "PlaybackListener - Publish new message";
        dataReceived(type, nextMessage);

        const quint64 timestamp = CanMessageParser::getTimeStamp(nextMessage);

        nextMessage = file.read(messageSize);
        const qint32 timeDiff = static_cast<qint32>(CanMessageParser::getTimeStamp(nextMessage) - timestamp);

        timer.singleShot(timeDiff, this, &PlaybackListener::receiveData);
    }
    else
    {
        qDebug() << "PlaybackListener - File ended";
        // reached the end of the file, perhaps the user should be notified
    }
}
//--------------------------------------------------------------------------------------------------
void PlaybackListener::configure()
{
    qDebug() << "PlaybackListener - Setting instance";
    SystemConfiguration* const manager = SystemConfiguration::instance();

    const QString filename = manager->getPlaybackFilename();

    if (manager->getListenerType() != type)
    {
        if (file.isOpen())
        {
            file.close();
        }
    }
    else if (file.fileName() != filename || !file.isOpen())
    {
        if (file.isOpen())
        {
            file.close();
        }

        file.setFileName(filename);
        file.open(QIODevice::ReadOnly);

        nextMessage = file.read(messageSize);
        receiveData();
    }

    qDebug() << "PlaybackListener - Instance configured";
}
//--------------------------------------------------------------------------------------------------

#ifndef MESSAGE_LISTENER_H
#define MESSAGE_LISTENER_H

#include <QObject>
#include <QByteArray>
#include "data-source-manager.h"
#include "system-configuration.h"
#include "can-message/can-message-parser.h"

class IMessageListener: public QObject
{
    Q_OBJECT
    public:
        explicit IMessageListener(QObject* parent): QObject(parent)
        {
            DataSourceManager* dataManager = DataSourceManager::instance();

            connect(this, &IMessageListener::dataReceived,
                    dataManager, &DataSourceManager::newData);
            messageSize = CanMessageParser::messageSize();

            connect(SystemConfiguration::instance(), &SystemConfiguration::configurationChanged,
                    this, &IMessageListener::configure);
        }

    protected:
        quint32 messageSize = 0;

        virtual void receiveData() = 0;

    signals:
        void dataReceived(ListenerType type, QByteArray data);

    public slots:
        virtual void configure() = 0;

};

#endif // MESSAGE_LISTENER_H

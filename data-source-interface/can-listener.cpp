#include "can-listener.h"
#include <QDebug>
#include <QStringList>
#include <QTextStream>
#include "error-widget.h"
//--------------------------------------------------------------------------------------------------
static CANListener* inst{nullptr};
//--------------------------------------------------------------------------------------------------
CANListener* CANListener::instance()
{
    if(nullptr == inst)
    {
        qDebug() << "CANListener - Instantiating";
        inst = new CANListener;
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
CANListener::CANListener(QWidget* parent) : IMessageListener(parent)
{
    configure();
    qDebug() << "CANListener - Instance created";
}
//--------------------------------------------------------------------------------------------------
CANListener::~CANListener()
{
    qDebug() << "CANListener - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------
void CANListener::closePort()
{
    if (serialPort.isOpen())
    {
        sendXOFF();
        serialPort.close();
    }
    qDebug() << "CANListener - Port closed";
}
//--------------------------------------------------------------------------------------------------
void CANListener::sendXOFF()
{
    QByteArray data;
    data.append("\x19");
    serialPort.write(data);
    qDebug() << "CANListener - XOFF sent";
}
//--------------------------------------------------------------------------------------------------
void CANListener::sendXON()
{
    QByteArray data;
    data.append("\x17");
    serialPort.write(data);
    qDebug() << "CANListener - XON sent";
}
//--------------------------------------------------------------------------------------------------
void CANListener::configure()
{
    qDebug() << "CANListener - Setting instance";
    SystemConfiguration* manager = SystemConfiguration::instance();

    const QString portName = manager->getPortLocation();
    const qint32 baudRate = static_cast<qint32>(manager->getBaudRate());

    ErrorWidget* const errorManager = ErrorWidget::instance();
    connect(this, &CANListener::publishError,
            errorManager, &ErrorWidget::publishError,
            Qt::UniqueConnection);

    if (manager->getListenerType() != type)
    {
        closePort();
    }
    else if (portName != serialPort.portName() || baudRate != serialPort.baudRate() || !serialPort.isOpen())
    {
        closePort();

        serialPort.setPortName(portName);
        serialPort.setBaudRate(baudRate);
        serialPort.setDataBits(QSerialPort::Data8);
        serialPort.setParity(QSerialPort::NoParity);
        serialPort.setStopBits(QSerialPort::OneStop);

        if (!serialPort.open(QIODevice::ReadWrite))
        {
            emit publishError("Failed to configure serial CAN listener - " + serialPort.errorString());
        }
        else
        {
            connect(&serialPort, &QSerialPort::readyRead,
                    this, &CANListener::receiveData,
                    Qt::UniqueConnection);

            connect(&serialPort, &QSerialPort::errorOccurred,
                    this, &CANListener::handleError,
                    Qt::UniqueConnection);

            sendXOFF();
            sendXON();
        }
    }
}
//--------------------------------------------------------------------------------------------------
void CANListener::receiveData()
{
    qDebug() << "CANListener - Data received";
    static QByteArray readData;

    readData.append(serialPort.readAll());

    qint32 msgSize = static_cast<qint32>(messageSize);
    while (readData.size() >= msgSize)
    {
        qDebug() << "CANListener - Valid message received";
        QByteArray message = readData.left(msgSize);
        readData.remove(0, msgSize);
        emit dataReceived(type, message);
    }
}
//--------------------------------------------------------------------------------------------------
void CANListener::handleError(QSerialPort::SerialPortError serialPortError)
{
    if (serialPortError == QSerialPort::ReadError)
    {
        qDebug() << "CANListener - Error occurred";
        qDebug() << "An I/O error occurred while reading " << serialPort.portName()
                 << "the data from port error: " << serialPort.errorString();
    }
}
//--------------------------------------------------------------------------------------------------

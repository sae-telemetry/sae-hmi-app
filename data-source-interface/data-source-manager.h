#ifndef DATA_SOURCE_MANAGER_H
#define DATA_SOURCE_MANAGER_H

#include <QWidget>
#include <QMap>
#include "system-configuration.h"

class DataSourceManager: public QObject
{
    Q_OBJECT
    public:
        static DataSourceManager* instance();

    private:
        explicit DataSourceManager(QObject* parent = nullptr);
        ~DataSourceManager();

        ListenerType currentListener = DISABLED;
        quint32 lastSeqNumber = 0;
        bool reconfigured = false;

    signals:
        void dataReceived(const quint32 id, const quint32 signalID, const quint64 timestamp,
                          const qreal payload);

        void publishError(QString error);

    public slots:
        void newData(const ListenerType type, QByteArray data);
        void configurationChanged();
};

#endif // DATA_SOURCE_MANAGER_H

#include "data-source-manager.h"
#include "can-message/can-message.h"
#include "can-message/can-dbc-manager.h"
#include "can-message/can-message-parser.h"
#include "error-widget.h"
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static DataSourceManager* inst{nullptr};
//--------------------------------------------------------------------------------------------------
DataSourceManager* DataSourceManager::instance()
{
    if(nullptr == inst)
    {
        qDebug() << "DataSourceManager - Instantiating";
        inst = new DataSourceManager;
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
DataSourceManager::DataSourceManager(QObject* parent) : QObject(parent)
{
    SystemConfiguration* inst = SystemConfiguration::instance();
    connect(inst, &SystemConfiguration::configurationChanged,
            this, &DataSourceManager::configurationChanged);
    qDebug() << "DataSourceManager - Instance createad";
}
//--------------------------------------------------------------------------------------------------
void DataSourceManager::newData(const ListenerType type, QByteArray data)
{
    if (currentListener == type)
    {
        qDebug() << "DataSourceManager - New data received";
        CanDbcManager* manager = CanDbcManager::instance();
        if (nullptr != manager && manager->isConfigured())
        {
            std::shared_ptr<CAN_Message> message = CanMessageParser::parseMessage(data);

            if (!reconfigured)
            {
                quint32 seqNumberDiff = message->SeqNumber - lastSeqNumber;
                if (seqNumberDiff > 1)
                {
                    emit publishError(tr("Lost %1 message(s) - between messages %2 and %3").arg(
                                          seqNumberDiff - 1).arg(lastSeqNumber).arg(message->SeqNumber));
                }
            }
            lastSeqNumber = message->SeqNumber;

            if(message->ErrorCode != 0)
            {
                // message will be dropped
                qDebug() << "DataSourceManager - Error occurred";
                emit publishError(tr("Occurred an error in message %1 - error code %2").arg(
                                      message->SeqNumber).arg(message->ErrorCode));
                return;
            }

            if (!CanMessageParser::validateCRC(data))
            {
                // message will be dropped
                qDebug() << "DataSourceManager - Invalid CRC";
                emit publishError(tr("CRC validation for message %1").arg(message->SeqNumber));
                return;
            }

            qDebug() << "DataSourceManager - Valid message received";

            const quint32 id = message->canID;
            const quint64 timestamp = CanMessageParser::getTimeStamp(message);

            try
            {
                const QMap<quint32, qreal> values = manager->getSignalValues(message->canID, message->CAN_Payload);
                QMapIterator<quint32, qreal> it(values);
                while(it.hasNext())
                {
                    it.next();
                    emit dataReceived(id, it.key(), timestamp, it.value());
                }
            }
            catch (...)
            {
                // error occurred while parsing signals in received message
            }
        }
    }
    else
    {
        // invalid listener
        qDebug() << "DataSourceManager - Received data from invalid source";
    }

    reconfigured = false;
}
//--------------------------------------------------------------------------------------------------
void DataSourceManager::configurationChanged()
{
    qDebug() << "DataSourceManager - Configuration changed";
    currentListener = SystemConfiguration::instance()->getListenerType();
    reconfigured = true;

    ErrorWidget* manager = ErrorWidget::instance();
    connect(this, &DataSourceManager::publishError,
            manager, &ErrorWidget::publishError,
            Qt::UniqueConnection);
}
//--------------------------------------------------------------------------------------------------
DataSourceManager::~DataSourceManager()
{
    currentListener = DISABLED;

    qDebug() << "DataSourceManager - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------

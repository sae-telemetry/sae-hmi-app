#include "main-sae-hmi-app.h"
#include "ui_main-sae-hmi-app.h"
#include "telemetry-widget.h"
#include "pilot-view.h"
#include "system-configuration.h"
#include "error-widget.h"
#include "can-message/can-dbc-manager.h"
#include <QPushButton>
#include <QLabel>
#include <QDebug>
#include "data-source-interface/playback-listener.h"
#include "data-source-interface/can-listener.h"
//--------------------------------------------------------------------------------------------------
MainSAEHMIApp::MainSAEHMIApp(QWidget* parent): QWidget{parent}, _ui{new Ui::MainSAEHMIApp}
{
    _ui->setupUi(this);

    initializeSystemConfiguration();
    initializeErrorWidget();
    connect(CanDbcManager::instance(), &CanDbcManager::configured,
            this, &MainSAEHMIApp::initializeTests);
    initializeTests();
//    initializePilotView();

    _ui->mainStackedWindow->setCurrentIndex(0);

    //todo: find better place to instantiate sources
    PlaybackListener::instance();
    CANListener::instance();
    qDebug() << "MainSAEHMIApp - Instance created";
}
//--------------------------------------------------------------------------------------------------
MainSAEHMIApp::~MainSAEHMIApp()
{
    delete _ui;
    qDebug() << "MainSAEHMIApp - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------
QWidget* MainSAEHMIApp::addPageToStackedWindow(const QString& windowTitle)
{
    TelemetryWidget* const wid{new TelemetryWidget(_ui->mainStackedWindow, windowTitle)};

    addPageToStackedWindow(wid);

    return wid;
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::addPageToStackedWindow(QWidget* const wid)
{
    const QString titleFieldName{"lblTitle"};

    if(wid != nullptr)
    {
        QLabel* const lblTitle{wid->findChild<QLabel*>(titleFieldName)};

        const QString windowTitle{(lblTitle != nullptr) ? lblTitle->text() : ""};

        QPushButton* const btn{new QPushButton(windowTitle, this)};
        btn->setObjectName("menuButton");

        connect(btn, &QPushButton::clicked, this, [=]()
        {
            _ui->mainStackedWindow->setCurrentWidget(wid);
        });

        TelemetryWidget* const testWindow{dynamic_cast<TelemetryWidget*>(wid)};
        if(testWindow != nullptr)
        {
            connect(_ui->mainStackedWindow, &QStackedWidget::currentChanged,
                    testWindow, &TelemetryWidget::visibilityChanged);
        }

        _ui->layButtons->insertWidget(0, btn);
        _ui->mainStackedWindow->addWidget(wid);
        qDebug() << "MainSAEHMIApp - Window added";
    }
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::removeTelemetryWidgets()
{
    qDebug() << "MainSAEHMIApp - Configured windows removed";
    for (qint32 idx = _ui->mainStackedWindow->count(); idx >= 0; idx--)
    {
        QWidget* wid = _ui->mainStackedWindow->widget(idx);
        if(TelemetryWidget* telemetry = qobject_cast<TelemetryWidget*>(wid))
        {
            _ui->mainStackedWindow->removeWidget(wid);
            telemetry->deleteLater();
        }
    }
    return;
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::initializePilotView()
{
    addPageToStackedWindow(new PilotView);
    qDebug() << "MainSAEHMIApp - Pilot view initialized";
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::initializeTests()
{
    CanDbcManager* const manager = CanDbcManager::instance();

    if (nullptr != manager && manager->isConfigured())
    {
        if (manager->isConfigured())
        {
            removeTelemetryWidgets();
        }

        const QList<QString> nodes = manager->getNodes();
        foreach (const QString& node, nodes)
        {
            const QList<quint32> messages = manager->getMessages(node);
            if(messages.isEmpty())
            {
                continue;
            }

            TelemetryWidget* const widTest{dynamic_cast<TelemetryWidget*>(addPageToStackedWindow(node))};

            foreach (const quint32 id, messages)
            {
                const QMap<quint32, QPair<QString, QString>> signalsMap= manager->getSignals(id);
                QMapIterator<quint32, QPair<QString, QString>> it(signalsMap);
                while(it.hasNext())
                {
                    it.next();
                    const QPair<QString, QString> pair = it.value();
                    const QString signalName = pair.first;
                    const QString signalUnit = pair.second;
                    widTest->addPlot(signalName, id, it.key(), signalUnit);
                }
            }

            widTest->mountView();
        }
    }

    qDebug() << "MainSAEHMIApp - Telemetry plots initialized";
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::initializeSystemConfiguration()
{
    addPageToStackedWindow(SystemConfiguration::instance(this));
    qDebug() << "MainSAEHMIApp - System configuration view initialized";
}
//--------------------------------------------------------------------------------------------------
void MainSAEHMIApp::initializeErrorWidget()
{
    ErrorWidget* manager{ErrorWidget::instance(this)};
    addPageToStackedWindow(manager);
    connect(manager, &ErrorWidget::showErrors, this, [=]()
    {
        _ui->mainStackedWindow->setCurrentWidget(manager);
    });
    qDebug() << "MainSAEHMIApp - Error view initialized";
}
//--------------------------------------------------------------------------------------------------

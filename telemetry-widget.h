#ifndef BASETESTWINDOW_H
#define BASETESTWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QList>

namespace Ui
{
    class TelemetryWidget;
}

class TelemetryWidget : public QWidget
{
    Q_OBJECT
    public:
        explicit TelemetryWidget(QWidget* parent, const QString& title);
        ~TelemetryWidget();

        void addPlot(const QString& title, const quint32 id, const quint32 signalID,
                     const QString& unit);
        void setTitle(const QString& title);
        void mountView();

    private:
        Ui::TelemetryWidget* ui;
        QList<QWidget*> _plots;

    signals:
        void visibilityChanged();

    private slots:
        void on_btnNextPage_clicked();
        void on_btnPreviousPage_clicked();
};

#endif // BASETESTWINDOW_H

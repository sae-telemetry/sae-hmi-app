#ifndef BASEPLOT_H
#define BASEPLOT_H

#include <QWidget>

namespace Ui
{
    class BasePlot;
}

class BasePlot : public QWidget
{
    Q_OBJECT
    public:
        BasePlot(const QString& title, const quint32 id, const quint32 signalID, QWidget* parent);
        ~BasePlot();

        void setTitle(const QString& title);

        quint32 getID() const;

        const QString& getTitle() const;

    private:
        Ui::BasePlot* _ui;
        quint32 _id;
        quint32 _signalID;
        QString _title;
        QVector<qreal> _dataX;
        QVector<qreal> _dataY;

        static qreal findMinimum(const QVector<qreal>& data);
        static qreal findMaximum(const QVector<qreal>& data);

    signals:

    public slots:
        void newDataReceived(const quint32 id, const quint32 signalID, const quint64 timestamp,
                             const qreal payload);

        void updatePlot();
};

#endif // BASEPLOT_H

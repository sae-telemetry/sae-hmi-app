#include "telemetry-widget.h"
#include "ui_telemetry-widget.h"
#include "base-plot.h"
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static const qint32 SIZE{2};
static const qint32 ITEMS_BY_PAGE{SIZE*SIZE};
//--------------------------------------------------------------------------------------------------
TelemetryWidget::TelemetryWidget(QWidget* parent, const QString& title):
                                                        QWidget{parent}, ui{new Ui::TelemetryWidget}
{
    ui->setupUi(this);
    setTitle(title);
    ui->btnNextPage->setVisible(false);
    ui->btnPreviousPage->setVisible(false);
    qDebug() << "TelemetryWidget - Instance created";
}
//--------------------------------------------------------------------------------------------------
TelemetryWidget::~TelemetryWidget()
{
    delete ui;
    qDebug() << "TelemetryWidget - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------
void TelemetryWidget::addPlot(const QString& title, const quint32 id, const quint32 signalID, const QString& unit)
{
    (void) unit;
    BasePlot* newPlot{new BasePlot(title, id, signalID, this)};

    _plots.push_back(newPlot);

    connect(this, &TelemetryWidget::visibilityChanged, newPlot, &BasePlot::updatePlot);
    qDebug() << "TelemetryWidget - Plot added (" << QString::number(id) << "," <<
                QString::number(signalID) << ")";
}
//--------------------------------------------------------------------------------------------------
void TelemetryWidget::setTitle(const QString& title)
{
    ui->lblTitle->setText(title);
    qDebug() << "TelemetryWidget - Title set to " << title;
}
//--------------------------------------------------------------------------------------------------
void TelemetryWidget::mountView()
{
    qint32 pageIdx{0};
    qint32 idx{0};

    QGridLayout* layout{nullptr};

    for(QWidget* const basePlot : _plots)
    {
        if(ITEMS_BY_PAGE <= pageIdx)
        {
            pageIdx = 0;
            layout = nullptr;
        }

        if(nullptr == layout)
        {
            QWidget* wid{new QWidget(ui->stackedWidget)};
            layout = new QGridLayout(wid);
            wid->setLayout(layout);
            ui->stackedWidget->addWidget(wid);
        }

        qint32 rowSpan{1};
        qint32 colSpan{1};
        qint32 x{pageIdx/SIZE};
        qint32 y{pageIdx%SIZE};


        if(_plots.size() == idx+1)
        {
            if(layout->count() == 2)
            {
                colSpan++;
            }

            if(layout->count() == 1)
            {
                x = 1;
                y = 0;
            }
        }

        layout->addWidget(basePlot, x, y, rowSpan, colSpan);

        idx++;
        pageIdx++;
    }

    ui->btnNextPage->setVisible(ui->stackedWidget->count() > 1);
    ui->btnPreviousPage->setVisible(ui->stackedWidget->count() > 1);
    qDebug() << "TelemetryWidget - View mounted";
}
//--------------------------------------------------------------------------------------------------
void TelemetryWidget::on_btnNextPage_clicked()
{
    const qint32 numberOfPages{ui->stackedWidget->count()};

    if(numberOfPages > 1)
    {
        const qint32 newPage{ui->stackedWidget->currentIndex() + 1};
        (numberOfPages > newPage) ?
                     ui->stackedWidget->setCurrentIndex(newPage) : ui->stackedWidget->setCurrentIndex(0);

        emit visibilityChanged();
        qDebug() << "TelemetryWidget - Next page button clicked";
    }
}
//--------------------------------------------------------------------------------------------------
void TelemetryWidget::on_btnPreviousPage_clicked()
{
    const qint32 numberOfPages{ui->stackedWidget->count()};

    if(numberOfPages > 1)
    {
        const qint32 newPage{ui->stackedWidget->currentIndex() - 1};
        (0 <= newPage) ? ui->stackedWidget->setCurrentIndex(newPage) :
                        ui->stackedWidget->setCurrentIndex(numberOfPages - 1);

        emit visibilityChanged();
        qDebug() << "TelemetryWidget - Previous page button clicked";
    }
}
//--------------------------------------------------------------------------------------------------

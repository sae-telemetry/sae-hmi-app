#include "base-plot.h"
#include "ui_base-plot.h"
#include "data-source-interface/data-source-manager.h"
#include <QDateTime>
#include <QString>
#include <algorithm>
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static const qint32 MAX_DATA_SIZE{600};
//--------------------------------------------------------------------------------------------------
BasePlot::BasePlot(const QString& title, const quint32 id, const quint32 signalID, QWidget* parent):
    QWidget{parent}, _ui{new Ui::BasePlot}, _id{id}, _signalID{signalID}
{
    _ui->setupUi(this);
    setTitle(title);

    _ui->widPlot->addGraph();
    _ui->widPlot->setBackground(QColor(80, 80, 80));

    DataSourceManager* dataSource{DataSourceManager::instance()};
    connect(dataSource, &DataSourceManager::dataReceived,
            this, &BasePlot::newDataReceived, Qt::UniqueConnection);

    qDebug() << "BasePlot instance created (" << QString::number(_id) << "," <<
                QString::number(_signalID) << ")";
}
//--------------------------------------------------------------------------------------------------
BasePlot::~BasePlot()
{
    delete _ui;
    qDebug() << "BasePlot instance destroyed (" << QString::number(_id) << "," <<
                QString::number(_signalID) << ")";
}
//--------------------------------------------------------------------------------------------------
void BasePlot::setTitle(const QString &title)
{
    qDebug() << "BasePlot instance title changed to " << title;
    _title = title;
    _ui->lblTitle->setText(title);
}
//--------------------------------------------------------------------------------------------------
quint32 BasePlot::getID() const
{
    return _id;
}
//--------------------------------------------------------------------------------------------------
const QString& BasePlot::getTitle() const
{
    return _title;
}
//--------------------------------------------------------------------------------------------------
qreal BasePlot::findMinimum(const QVector<qreal>& data)
{
    return *std::min_element(data.constBegin(), data.constEnd());
}
//--------------------------------------------------------------------------------------------------
qreal BasePlot::findMaximum(const QVector<qreal> &data)
{
    return *std::max_element(data.constBegin(), data.constEnd());
}
//--------------------------------------------------------------------------------------------------
void BasePlot::newDataReceived(const quint32 id, const quint32 signalID, const quint64 timestamp,
                               const qreal payload)
{
    static quint64 startTime{timestamp};

    if(id == _id && signalID == _signalID)
    {
        qDebug() << "BasePlot instance (" << QString::number(id) << "," <<
                QString::number(signalID) << ") new data";
        const qreal time{static_cast<qreal>(timestamp - startTime)};

        if(_dataY.size() >= MAX_DATA_SIZE)
        {
            _dataX.pop_front();
            _dataY.pop_front();
        }

        _dataX.push_back(time);
        _dataY.push_back(payload);

        updatePlot();
        const quint64 currMsecs = static_cast<quint64>(QDateTime::currentMSecsSinceEpoch());
        qDebug() << "Message " + QString::number(id) + " signal ID " + QString::number(signalID) +
                    " with time difference of " + QString::number(currMsecs - timestamp) + " ms";
    }
}
//--------------------------------------------------------------------------------------------------
void BasePlot::updatePlot()
{
    if(isVisible() && !_dataY.empty())
    {
        _ui->widPlot->graph(0)->setData(_dataX, _dataY, true);

        _ui->widPlot->xAxis->setRange(findMinimum(_dataX), findMaximum(_dataX));
        _ui->widPlot->yAxis->setRange(findMinimum(_dataY), findMaximum(_dataY));

        _ui->leCurrentValue->setText(QString::number(_dataY.last()));

        _ui->widPlot->replot();

        qDebug() << "BasePlot instance (" << QString::number(_id) << "," <<
                QString::number(_signalID) << ") updated";
    }
}
//--------------------------------------------------------------------------------------------------

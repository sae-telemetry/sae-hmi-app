#include <dbcppp/Network.h>
#include "can-dbc-manager.h"
#include <fstream>
#include <QSet>
#include "system-configuration.h"
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static CanDbcManager* inst{nullptr};
//--------------------------------------------------------------------------------------------------
CanDbcManager* CanDbcManager::instance()
{
    if(nullptr == inst)
    {
        qDebug() << "Instantiating CanDbcManager";
        inst = new CanDbcManager;
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
QList<QString> CanDbcManager::getNodes() const
{
    qDebug() << "CanDbcManager - Get nodes";
    QList<QString> ret;

    network->forEachNode([&](const dbcppp::Node& node)
    {
        ret << QString::fromStdString(node.getName());
    });

    return ret;
}
//--------------------------------------------------------------------------------------------------
QList<quint32> CanDbcManager::getMessages(const QString node) const
{
    qDebug() << "CanDbcManager - Get messages";
    QList<quint32> ret;

    network->forEachMessage([&](const dbcppp::Message& message)
    {
        const QString messageNode = QString::fromStdString(message.getTransmitter());
        if (messageNode == node)
        {
            ret << static_cast<quint32>(message.getId());
        }
    });

    return ret;
}
//--------------------------------------------------------------------------------------------------
QMap<quint32, QPair<QString, QString>> CanDbcManager::getSignals(const quint32 messageID) const
{
    qDebug() << "CanDbcManager - Get signals";
    QMap<quint32, QPair<QString, QString>> ret;

    const dbcppp::Message* const message = network->getMessageById(messageID);
    message->forEachSignal([&](const dbcppp::Signal& signal)
    {
        const quint32 startBit = static_cast<quint32>(signal.getStartBit());
        const QString name = QString::fromStdString(signal.getName());
        const QString unit = QString::fromStdString(signal.getUnit());
        QPair<QString, QString> pair(name, unit);

        ret.insert(startBit, pair);
    });

    return ret;
}
//--------------------------------------------------------------------------------------------------
QMap<quint32, qreal> CanDbcManager::getSignalValues(const quint32 messageID, const quint8 data[8]) const
{
    qDebug() << "CanDbcManager - Get signal values";
    QMap<quint32, qreal> ret;

    const dbcppp::Message* const message = network->getMessageById(messageID);
    message->forEachSignal([&](const dbcppp::Signal& signal)
    {
        const quint64 raw = signal.decode(data);
        const qreal value = signal.rawToPhys(raw);
        const quint32 startBit = static_cast<quint32>(signal.getStartBit());

        ret.insert(startBit, value);
    });

    return ret;
}
//--------------------------------------------------------------------------------------------------
bool CanDbcManager::isConfigured()
{
    const QString isConfiguredStr = _configured ? "" : "not ";
    qDebug() << "CanDbcManager - isConfigured called: " << isConfiguredStr << "configured";
    return _configured;
}
//--------------------------------------------------------------------------------------------------
CanDbcManager::CanDbcManager(QObject* parent): QObject(parent)
{
    configure();
    connect(SystemConfiguration::instance(), &SystemConfiguration::configurationChanged,
            this, &CanDbcManager::configure);

    qDebug() << "CanDbcManager instance created";
}
//--------------------------------------------------------------------------------------------------
CanDbcManager::~CanDbcManager()
{
    disconnect(SystemConfiguration::instance(), &SystemConfiguration::configurationChanged,
            this, &CanDbcManager::configure);

    qDebug() << "CanDbcManager instance destroyed";
}
//--------------------------------------------------------------------------------------------------
void CanDbcManager::configure()
{
    const QString filename = SystemConfiguration::instance()->getDBCFilename();
    if (!filename.isEmpty() && filename != dbcFileName)
    {
        qDebug() << "CanDbcManager - Configuring instance";
        dbcFileName = filename;
        std::ifstream dbc_file{dbcFileName.toStdString()};
        network = dbcppp::Network::loadDBCFromIs(dbc_file);
        _configured = true;
        emit configured();
    }
}
//--------------------------------------------------------------------------------------------------

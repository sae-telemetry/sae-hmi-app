#include "can-message-parser.h"
#include <QDateTime>
#include <QTime>
#include <QDate>
#include <crc.hpp>
#include <QtEndian>
#include <QDebug>
//--------------------------------------------------------------------------------------------------
std::shared_ptr<CAN_Message> CanMessageParser::parseMessage(char* const binMessage)
{
    QByteArray data;
    data.append(binMessage, static_cast<qint32>(messageSize()));
    return CanMessageParser::parseMessage(data);
}
//--------------------------------------------------------------------------------------------------
std::shared_ptr<CAN_Message> CanMessageParser::parseMessage(QByteArray& binMessage)
{
    CAN_Message* message = reinterpret_cast<CAN_Message*>(binMessage.data());
    qDebug() << "CanMessageParser - Parse message " << QString::number(message->SeqNumber);
    return std::make_shared<CAN_Message>(*message);
}
//--------------------------------------------------------------------------------------------------
uint64_t CanMessageParser::getTimeStamp(std::shared_ptr<CAN_Message> message)
{
    qDebug() << "CanMessageParser - Get message timestamp";
    const QDate date = QDate(static_cast<qint32>(message->Year),
                             static_cast<qint32>(message->Month),
                             static_cast<qint32>(message->Date));
    const QTime time = QTime(static_cast<qint32>(message->Hours),
                             static_cast<qint32>(message->Minutes),
                             static_cast<qint32>(message->Seconds),
                             static_cast<qint32>((1000*(255 - message->SubSeconds))/256));

    const QDateTime dateTime = QDateTime(date, time);
    return static_cast<quint64>(dateTime.toMSecsSinceEpoch());
}
//--------------------------------------------------------------------------------------------------
uint64_t CanMessageParser::getTimeStamp(char* const message)
{
    return getTimeStamp(parseMessage(message));
}
//--------------------------------------------------------------------------------------------------
uint64_t CanMessageParser::getTimeStamp(QByteArray& message)
{
    return getTimeStamp(parseMessage(message));
}
//--------------------------------------------------------------------------------------------------
uint32_t CanMessageParser::messageSize()
{
    qDebug() << "CanMessageParser - Get message size";
    const quint32 size = static_cast<quint32>(sizeof(CAN_Message));
    return size;
}
//--------------------------------------------------------------------------------------------------
bool CanMessageParser::validateCRC(QByteArray& message)
{
    CAN_Message parsedMessage = *(CanMessageParser::parseMessage(message));
    qDebug() << "CanMessageParser - Validating message" <<
                QString::number(parsedMessage.SeqNumber) << "CRC";
    const quint32 size = CanMessageParser::messageSize() - 2 * static_cast<quint32>(sizeof(parsedMessage.CRC_Value));

    quint32* val = reinterpret_cast<quint32*>(&parsedMessage);
    for (quint32 idx = 0; idx < size/sizeof (quint32); idx++)
    {
        val[idx] = qToBigEndian(val[idx]);
    }

    boost::crc_optimal<32U, 0x04C11DB7, 0xFFFFFFFFU, 0x00000000U, false, false> crc;
    crc.process_bytes(reinterpret_cast<uint32_t*>(&parsedMessage), size);

    const bool isValid = crc.checksum() == parsedMessage.CRC_Value;
    const QString isValidStr = isValid ? "Valid" : "Invalid";
    qDebug() << isValidStr << "CRC (" << QString::number(crc.checksum()) << "," <<
                QString::number(parsedMessage.CRC_Value) << ") for message" <<
                QString::number(parsedMessage.SeqNumber);

    return isValid;
}
//--------------------------------------------------------------------------------------------------

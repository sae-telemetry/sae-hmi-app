#ifndef CAN_MESSAGE_PARSER_H
#define CAN_MESSAGE_PARSER_H

#include "can-message.h"
#include <QByteArray>
#include <memory>

class CanMessageParser
{
    public:
        static std::shared_ptr<CAN_Message> parseMessage(char* const binMessage);
        static std::shared_ptr<CAN_Message> parseMessage(QByteArray& binMessage);
        static uint64_t getTimeStamp(std::shared_ptr<CAN_Message> message);
        static uint64_t getTimeStamp(char* const message);
        static uint64_t getTimeStamp(QByteArray& message);
        static uint32_t messageSize();
        static bool validateCRC(QByteArray& message);
};

#endif // CAN_MESSAGE_PARSER_H

#ifndef CAN_DBC_MANAGER_H
#define CAN_DBC_MANAGER_H

#include <QObject>
#include <memory>
#include <QList>
#include <QMap>
#include <QPair>

namespace dbcppp
{
    class Network;
    class Message;
}

class CanDbcManager: public QObject
{
    Q_OBJECT
    public:
        static CanDbcManager* instance();

        QList<QString> getNodes() const;
        QList<quint32> getMessages(const QString node) const;
        QMap<quint32, QPair<QString, QString>> getSignals(const quint32 messageID) const;
        QMap<quint32, qreal> getSignalValues(const quint32 messageID, const quint8 data[8]) const;
        bool isConfigured();

    private:
        explicit CanDbcManager(QObject* parent = nullptr);
        ~CanDbcManager();

        std::unique_ptr<dbcppp::Network> network;
        QString dbcFileName;
        bool _configured = false;

    signals:
        void configured();

    private slots:
        void configure();

};

#endif // CAN_DBC_MANAGER_H

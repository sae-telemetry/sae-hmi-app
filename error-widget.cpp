#include "error-widget.h"
#include "ui_error-widget.h"
#include <QFile>
#include<QString>
#include <QPushButton>
#include <QDateTime>
#include <QDebug>
#include <QTextEdit>
//--------------------------------------------------------------------------------------------------
static ErrorWidget* inst{nullptr};
//--------------------------------------------------------------------------------------------------
ErrorWidget::ErrorWidget(QWidget* parent): QWidget(parent), ui(new Ui::ErrorWidget)
{
    ui->setupUi(this);

    ui->errors->horizontalHeader()->resizeSections(QHeaderView::ResizeToContents);

    QPushButton* connectButton = msgBox.addButton(tr("Show"), QMessageBox::NoRole);
    connect(connectButton, &QPushButton::clicked, this, &ErrorWidget::showErrors);

    msgBox.setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

    QFile file(":/styles/styles/style.qss");
    file.open(QFile::ReadOnly);
    QString stylesheet{QLatin1String(file.readAll())};
    msgBox.setStyleSheet(stylesheet);
    qDebug() << "ErrorWidget - Instance created";
}
//--------------------------------------------------------------------------------------------------
void ErrorWidget::addError(QString error)
{
    qint32 rowCount = ui->errors->rowCount();
    ui->errors->setRowCount(rowCount+1);

    QString currDateTime = QDateTime::currentDateTime().toString("dd/MM/yyyy hh:mm:ss");
    QTableWidgetItem* timestampItem = new QTableWidgetItem(currDateTime);
    timestampItem->setTextAlignment(Qt::AlignCenter);
    ui->errors->setItem(rowCount, 0, timestampItem);

    QTableWidgetItem* errorItem = new QTableWidgetItem(error);
    QTextEdit *edit = new QTextEdit();
    edit->setReadOnly(true);
    edit->setText(error);
    ui->errors->setItem(rowCount, 1, errorItem);

    ui->errors->resizeRowsToContents();
}
//--------------------------------------------------------------------------------------------------
void ErrorWidget::publishError(QString error)
{
    qDebug() << "ErrorWidget - New error";
    addError(error);

    msgBox.setText(error);
    msgBox.open();
    msgBox.move(window()->rect().bottomRight());

    timer.singleShot(2000, this, &ErrorWidget::notificationTimedOut);
}
//--------------------------------------------------------------------------------------------------
void ErrorWidget::notificationTimedOut()
{
    msgBox.close();
    qDebug() << "ErrorWidget - Notification box closed";
}
//--------------------------------------------------------------------------------------------------
ErrorWidget *ErrorWidget::instance(QWidget* parent)
{
    if(nullptr == inst)
    {
        qDebug() << "ErrorWidget - Instantiating";
        inst = new ErrorWidget(parent);
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
ErrorWidget::~ErrorWidget()
{
    qDebug() << "ErrorWidget - Instance destroyed";
}
//--------------------------------------------------------------------------------------------------
void ErrorWidget::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);

    qint32 colCount = ui->errors->columnCount();
    QHeaderView* const header = ui->errors->horizontalHeader();
    for (int idx = 0; idx < colCount; ++idx)
    {
        header->resizeSection(idx, ui->errors->size().width()/colCount);
    }
}
//--------------------------------------------------------------------------------------------------

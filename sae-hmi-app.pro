QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += serialport

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp
SOURCES += pilot-view.cpp
SOURCES += system-configuration.cpp
SOURCES += base-plot.cpp
SOURCES += telemetry-widget.cpp
SOURCES += qcustomplot/qcustomplot.cpp
SOURCES += main-sae-hmi-app.cpp
SOURCES += data-source-interface/data-source-manager.cpp
SOURCES += data-source-interface/can-listener.cpp
SOURCES += data-source-interface/playback-listener.cpp
SOURCES += can-message/can-dbc-manager.cpp
SOURCES += can-message/can-message-parser.cpp
SOURCES += error-widget.cpp

HEADERS += main-sae-hmi-app.h
HEADERS += pilot-view.h
HEADERS += system-configuration.h
HEADERS += base-plot.h
HEADERS += telemetry-widget.h
HEADERS += qcustomplot/qcustomplot.h
HEADERS += data-source-interface/data-source-manager.h
HEADERS += data-source-interface/can-listener.h
HEADERS += data-source-interface/playback-listener.h
HEADERS += data-source-interface/message-listener.h
HEADERS += can-message/can-message.h
HEADERS += can-message/can-dbc-manager.h
HEADERS += can-message/can-message-parser.h
HEADERS += error-widget.h

FORMS += main-sae-hmi-app.ui
FORMS += pilot-view.ui
FORMS += system-configuration.ui
FORMS += base-plot.ui
FORMS += telemetry-widget.ui
FORMS += error-widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += icons.qrc \
    styles.qrc

LIBS += -ldbcppp

# Include path for windows here (need to install boost library locally)
win32:INCLUDEPATH += "C:/libs/include/boost"
unix:INCLUDEPATH += "/usr/include/boost"

#!/bin/bash

# update repositories
apt update

# install Qt Framework and Qt Creator
apt install qtcreator -y
apt install build-essential -y
apt install qt5-default -y
apt install qt5-doc qt5-doc-html qtbase5-doc-html qtbase5-examples -y
apt install libqt5serialport5 -y
apt install libqt5serialport5-dev -y

# install lib boost
# if libboost1.74-dev not present, use the PPA from https://launchpad.net/~mhier/+archive/ubuntu/libboost-latest using the following command:
# sudo add-apt-repository ppa:mhier/libboost-latest
apt install libboost1.74-dev -y
apt install build-essential g++ python-dev autotools-dev libicu-dev libbz2-dev -y

# install libxml2
apt install libxml2 libxml2-dev -y

# install lib dbcppp
cd ..
git clone https://github.com/xR3b0rn/dbcppp.git
cd dbcppp
git checkout 73f53ed35fbafcb09abc2ad14d9fc8cad12c6c65
git switch -c build
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j
make RunTests
make install
ldconfig

# remove dbcppp repo from machine
cd ../../
rm -r dbcppp

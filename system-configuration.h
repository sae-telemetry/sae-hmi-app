#ifndef SYSTEMCONFIGURATION_H
#define SYSTEMCONFIGURATION_H

#include <QWidget>

enum ListenerType: quint8 {DISABLED,  RADIO, PLAYBACK};

namespace Ui
{
    class SystemConfiguration;
}

class SystemConfiguration: public QWidget
{
    Q_OBJECT

    public:
        static SystemConfiguration* instance(QWidget* parent = nullptr);
        ~SystemConfiguration();

        ListenerType getListenerType() const;
        quint32 getBaudRate() const;
        QString getPortLocation() const;
        QString getPlaybackFilename() const;
        QString getDBCFilename() const;

    private:
        explicit SystemConfiguration(QWidget* parent = nullptr);

        Ui::SystemConfiguration *ui;
        ListenerType listenerType = DISABLED;
        QString dbcFilename = "";
        quint32 baudRate = 9600;
        QString portLocation = "";
        QString playbackFilename = "";

    signals:
        void configurationChanged();

    private slots:
        void opModeChanged(const qint32 index);
        void applyConfiguration();
        void updateSerialPorts();
        void enableApplyButton();
        void browseDBCFile();
        void browsePbkFile();
};

#endif // SYSTEMCONFIGURATION_H

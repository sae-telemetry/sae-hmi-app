#include "system-configuration.h"
#include "ui_system-configuration.h"
#include <QSerialPortInfo>
#include <QTimer>
#include <QList>
#include <QDir>
#include <QFileDialog>
#include <QDebug>
//--------------------------------------------------------------------------------------------------
static SystemConfiguration* inst{nullptr};
//--------------------------------------------------------------------------------------------------
SystemConfiguration::SystemConfiguration(QWidget *parent) : QWidget(parent),
    ui(new Ui::SystemConfiguration)
{
    ui->setupUi(this);

    updateSerialPorts();

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &SystemConfiguration::updateSerialPorts);
    timer->start(1000);

    connect(ui->cbOpMode, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &SystemConfiguration::opModeChanged);

    connect(ui->cbOpMode, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &SystemConfiguration::enableApplyButton);
    connect(ui->cbPort, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &SystemConfiguration::enableApplyButton);
    connect(ui->cbBaudRate, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &SystemConfiguration::enableApplyButton);
    connect(ui->leDbcFilename, &QLineEdit::textChanged,
            this, &SystemConfiguration::enableApplyButton);
    connect(ui->leFilename, &QLineEdit::textChanged,
            this, &SystemConfiguration::enableApplyButton);

    connect(ui->btnBrowseDBCFile, &QPushButton::clicked, this, &SystemConfiguration::browseDBCFile);
    connect(ui->btnBrowsePbkFile, &QPushButton::clicked, this, &SystemConfiguration::browsePbkFile);

    enableApplyButton();

    connect(ui->btnApply, &QPushButton::clicked, this, &SystemConfiguration::applyConfiguration);
    qDebug() << "SystemConfiguration - Instance created";
}
//--------------------------------------------------------------------------------------------------
SystemConfiguration *SystemConfiguration::instance(QWidget* parent)
{
    if(nullptr == inst)
    {
        qDebug() << "SystemConfiguration - Instantiating";
        inst = new SystemConfiguration(parent);
    }

    return inst;
}
//--------------------------------------------------------------------------------------------------
SystemConfiguration::~SystemConfiguration()
{
    qDebug() << "SystemConfiguration - Instance destroyed";
    delete ui;
}
//--------------------------------------------------------------------------------------------------
ListenerType SystemConfiguration::getListenerType() const
{
    qDebug() << "SystemConfiguration - Get listener type";
    return listenerType;
}
//--------------------------------------------------------------------------------------------------
quint32 SystemConfiguration::getBaudRate() const
{
    qDebug() << "SystemConfiguration - Get baud rate";
    return baudRate;
}
//--------------------------------------------------------------------------------------------------
QString SystemConfiguration::getPortLocation() const
{
    qDebug() << "SystemConfiguration - Get port location";
    return portLocation;
}
//--------------------------------------------------------------------------------------------------
QString SystemConfiguration::getPlaybackFilename() const
{
    qDebug() << "SystemConfiguration - Get playback filename";
    return playbackFilename;
}
//--------------------------------------------------------------------------------------------------
QString SystemConfiguration::getDBCFilename() const
{
    qDebug() << "SystemConfiguration - Get dbc filename";
    return dbcFilename;
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::opModeChanged(const qint32 index)
{
    qDebug() << "SystemConfiguration - Operation mode changed to " << QString::number(index);
    ui->widConfigurations->setCurrentIndex(index);
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::applyConfiguration()
{
    listenerType = static_cast<ListenerType>(ui->cbOpMode->currentIndex());
    dbcFilename = ui->leDbcFilename->text();

    switch (listenerType)
    {
        case DISABLED:
            break;

        case PLAYBACK:
        {
            playbackFilename = ui->leFilename->text();
            break;
        }

        case RADIO:
        {
            const QString baudRateText = ui->cbBaudRate->currentText();
            baudRate = baudRateText.toUInt();
            portLocation = ui->cbPort->currentText().split(" ")[0];
            break;
        }
    }

    qDebug() << "SystemConfiguration - Configuration applied";

    emit configurationChanged();
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::updateSerialPorts()
{
    const QList<QSerialPortInfo> infos = QSerialPortInfo::availablePorts();

    QList<QString> availablePorts;
    for (const QSerialPortInfo& info : infos)
    {
        availablePorts.push_back(info.systemLocation());
    }

    const qint32 currIndex = ui->cbPort->currentIndex();
    for (qint32 idx = ui->cbPort->count(); idx > 0; idx--)
    {
        const QString portLocation = ui->cbPort->itemText(idx).split(" ")[0];
        const qint32 portIndex = availablePorts.indexOf(portLocation);
        if (portIndex == -1)
        {
            ui->cbPort->removeItem(idx);
            if(currIndex == idx)
            {
                ui->cbPort->setCurrentIndex(0);
            }
        }
        else
        {
            availablePorts.removeAt(portIndex);
        }
    }

    for (const QSerialPortInfo& info : infos)
    {
        if (availablePorts.contains(info.systemLocation()))
        {
            ui->cbPort->addItem(info.systemLocation() + " (" + info.manufacturer() + ")");
        }
    }

    qDebug() << "SystemConfiguration - Available serial ports updated";
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::enableApplyButton()
{
    bool enable = true;

    enable &= !ui->leDbcFilename->text().isEmpty();

    switch (ui->cbOpMode->currentIndex())
    {
        case DISABLED:
            break;

        case PLAYBACK:
        {
            enable &= !ui->leFilename->text().isEmpty();
            break;
        }

        case RADIO:
        {
            enable &= !ui->cbPort->currentText().isEmpty();
            enable &= !ui->cbBaudRate->currentText().isEmpty();
            break;
        }

        default:
            enable &= false;
            break;
    }

    const QString enabledStr = enable ? "enabled" : "disabled";
    qDebug() << "SystemConfiguration - Apply button " << enabledStr;

    ui->btnApply->setEnabled(enable);
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::browseDBCFile()
{
    const QString filename = QFileDialog::getOpenFileName(this, tr("Find File"), QDir::home().path());
    if (!filename.isNull() && !filename.isEmpty())
    {
        ui->leDbcFilename->setText(QDir::toNativeSeparators(filename));
        qDebug() << "SystemConfiguration - DBC file set to " << filename;
    }
}
//--------------------------------------------------------------------------------------------------
void SystemConfiguration::browsePbkFile()
{
    const QString filename = QFileDialog::getOpenFileName(this, tr("Find File"), QDir::home().path());
    if (!filename.isNull() && !filename.isEmpty())
    {
        ui->leFilename->setText(QDir::toNativeSeparators(filename));
        qDebug() << "SystemConfiguration - Playback file set to " << filename;
    }
}
//--------------------------------------------------------------------------------------------------

#ifndef ERROR_WIDGET_H
#define ERROR_WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QMessageBox>

namespace Ui
{
    class ErrorWidget;
}

class ErrorWidget: public QWidget
{
    Q_OBJECT

    public:
        static ErrorWidget* instance(QWidget* parent = nullptr);
        ~ErrorWidget();

    protected:
        void resizeEvent(QResizeEvent *event);

    private:
        explicit ErrorWidget(QWidget* parent = nullptr);

        void addError(QString error);

        Ui::ErrorWidget *ui;
        QTimer timer;
        QMessageBox msgBox;

    public slots:
        void publishError(QString error);

    private slots:
        void notificationTimedOut();

    signals:
        void showErrors();
};

#endif // ERROR_WIDGET_H
